from argparse import ArgumentParser
import os

from transformers import GPT2Tokenizer

from utils.utils import generate


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--output_dir', type=str, default='/results',
                        help='Path to folder to save text generation')
    parser.add_argument('--text_name', type=str, default='example', help='Name for text file')

    args = parser.parse_args()

    dataset_tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
    dataset_tokenizer.pad_token = dataset_tokenizer.eos_token
    predict = generate(dataset_tokenizer.bos_token, 'big')
    with open(os.path.join(args.output_dir, args.text_name + '.txt'), 'w') as f_txt:
        f_txt.write(predict)
