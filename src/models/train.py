from tqdm import tqdm
import numpy as np
import os
from argparse import ArgumentParser
from typing import Optional

import torch
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import MultiStepLR
from transformers import AdamW, Optimizer, PreTrainedModel, GPT2LMHeadModel


from src.data.dataset import ScriptsDataset
from src.utils.preprocessing import prep_data


def train(traindataloader: DataLoader, model: PreTrainedModel, optimizer: Optimizer,
          scheduler: MultiStepLR, device, epochs: int, history_dict: dict,
          output_dir: str, output_prefix: str, testdataloader: Optional[DataLoader],
          validate: bool = False):
    losses_train_overall = []
    if validate:
        losses_test_overall = []
    for i in range(epochs):
        print(f'Training epoch {i}')
        model.train()
        train_loss = []
        test_loss = []
        model.zero_grad()
        optimizer.zero_grad()
        for batch in tqdm(traindataloader, desc='train'):
            inputs, attention, labels = batch
            inputs = inputs.to(device)
            attention = attention.to(device)
            labels = labels.to(device)
            outputs = model(input_ids=inputs, attention_mask=attention, labels=labels)
            del inputs, attention, labels
            loss = outputs[0]
            batch_loss = loss.cpu().item()
            train_loss.append(batch_loss)
            loss.backward()
            optimizer.step()
            scheduler.step()
            model.zero_grad()
            optimizer.zero_grad()
            del outputs
        train_loss_mean = np.mean(train_loss)
        print(f'train loss = {train_loss_mean}')
        print()
        if validate:
            model.eval()
            optimizer.zero_grad()
            model.zero_grad()
            with torch.no_grad():
                for batch in tqdm(testdataloader, desc="eval"):
                    inputs, attention, labels = batch
                    inputs = inputs.to(device)
                    attention = attention.to(device)
                    labels = labels.to(device)
                    outputs = model(input_ids=inputs, attention_mask=attention, labels=labels)
                    loss = outputs[0]
                    batch_val_loss = loss.cpu().item()
                    test_loss.append(batch_val_loss)
                    del loss
                test_loss_mean = np.mean(test_loss)
                print(f'validate loss = {test_loss_mean}')
                print()
            torch.save(model.state_dict(), os.path.join(output_dir, f"{output_prefix}-{i}.pt"))
        losses_train_overall.append(train_loss_mean)
        if validate:
            losses_test_overall.append(test_loss_mean)
    history_dict['train_loss'].extend(losses_train_overall)
    if validate:
        history_dict['test_loss'].extend(losses_test_overall)
    torch.save(model.state_dict(), os.path.join(output_dir,
               f"{output_prefix}-{len(history_dict['train_loss'])}.pt"))
    return history_dict


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--output_dir', type=str, default='/models',
                        help='Path to folder to save model weights')
    parser.add_argument('--output_prefix', type=str, default='distilgpt2_best',
                        help='Prefix for name experiment')
    parser.add_argument('--path_data', type=str, default='/data',
                        help='Path to folder to data')

    args = parser.parse_args()

    file_list = ['django_unchained_script.txt',
                 'inglorious_basterds_script.txt',
                 'pulp_fiction_script.txt',
                 'reservoir_dogs_screenplay.txt']
    text_all, list_of_tokens_all = prep_data(args.path_data,
                                             file_list,
                                             max_len_of_sent=200,
                                             max_len_of_paragraph=200)
    text_dataset = ScriptsDataset(text_all)
    dataloader = DataLoader(text_dataset, batch_size=8, shuffle=False)

    model = GPT2LMHeadModel.from_pretrained('distilgpt2')
    optimizer = AdamW(model.parameters(), lr=0.0001)
    scheduler = MultiStepLR(optimizer, milestones=[3, 7], gamma=0.9)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    history: dict = {'train_loss': [],
                     'test_loss': []}

    history = train(traindataloader=dataloader,
                    model=model,
                    optimizer=optimizer,
                    scheduler=scheduler,
                    device=device,
                    epochs=10,
                    history_dict=history,
                    output_dir=args.output_dir,
                    output_prefix=args.output_prefix,
                    testdataloader=None,
                    validate=False)

    # TODO: save history to json
