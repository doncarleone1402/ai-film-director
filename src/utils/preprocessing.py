import os
from typing import Tuple
from pathlib import Path

from nltk.tokenize import sent_tokenize
from transformers import GPT2Tokenizer


def prep_data(path_to_data: str, list_of_true_names: list, max_len_of_sent: int,
              max_len_of_paragraph: int) -> Tuple[list, list]:
    list_of_files: list = [
        os.path.join(Path.cwd().parent, path_to_data, i) for i in list_of_true_names
    ]
    text: str = ''
    for file in list_of_files:
        with open(file, 'r') as f:
            if text == '':
                text = f.read()
            else:
                text += f.read()
    text = text.replace('\n', ' ')
    text = text.replace('***', '')
    text = text.replace('“', "'")
    text = text.replace('”', "'")
    text = text.replace('’', "'")
    text = text.replace('‘', "'")
    text = sent_tokenize(text)
    print('Top 20 the longest sent: ' + str(sorted([len(sent.split()) for sent in text],
          reverse=True)[:20]))
    print(f'Deleteing sentences with more than {max_len_of_sent} words...')
    list_of_lens: list = [
        len(sent.split()) for sent in text if len(sent.split()) < max_len_of_sent
    ]
    text_lst: list = [sent for sent in text if len(sent.split()) < max_len_of_sent]
    print(f'Perform paragraphs with less than {max_len_of_paragraph} words from sentences...')
    list_of_paragraph_lens: list = []
    improved_text_array: list = []
    i = 0
    list_of_paragraph_lens.append([list_of_lens[0]])
    improved_text_array.append(text_lst[0])
    for j in range(1, len(list_of_lens)-2):
        if sum(list_of_paragraph_lens[i]) + list_of_lens[j] < max_len_of_paragraph:
            list_of_paragraph_lens[i].append(list_of_lens[j])
            improved_text_array[i] += ' ' + text_lst[j]
        else:
            list_of_paragraph_lens.append([list_of_lens[j]])
            improved_text_array.append(text_lst[j])
            i += 1
    improved_text_array[i] += ' ' + text_lst[-1]

    tokenizer = GPT2Tokenizer.from_pretrained('distilgpt2')
    tokenizer.pad_token = tokenizer.eos_token
    list_of_tokens: list = [len(tokenizer.encode(sent)) for sent in improved_text_array]
    print('top 20 the longest tokens: ' + str(sorted(list_of_tokens, reverse=True)[:20]))
    print('top 20 the smallest tokens: ' + str(sorted(list_of_tokens)[:20]))
    return improved_text_array, list_of_tokens
