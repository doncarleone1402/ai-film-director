from typing import Tuple
import glob
import re

import torch
from transformers import PreTrainedTokenizer, PreTrainedModel


def generate(text: str, dataset_tokenizer: PreTrainedTokenizer, model: PreTrainedModel,
             device: torch.device, size: str = 'small', do_sample: bool = True,
             early_stopping: bool = True, num_beans: int = 3,
             temperature: float = 0.8, top_k: int = 40, top_p: float = 0.95,
             num_return_sequences: int = 1) -> str:
    """
    Generate text in inference mode
    """
    dict_of_size: dict = {'extrasmall': 50, 'small': 100, 'middle': 150, 'big': 200}
    ids = dataset_tokenizer.encode(text, return_tensors='pt').to(device)
    greedy_output = model.generate(
            ids,
            max_length=dict_of_size[size],
            do_sample=do_sample,
            early_stopping=early_stopping,
            num_beans=num_beans,
            temperature=temperature,
            top_k=top_k,
            top_p=top_p,
            num_return_sequences=num_return_sequences)
    result = dataset_tokenizer.decode(greedy_output[0], skip_special_tokens=True)
    return result


def get_last_state_dict(path_to_data: str) -> Tuple[str, int]:
    list_of_files = glob.glob(f'{path_to_data}/*')
    array = []
    for file in list_of_files:
        array.append((file, int(re.findall(r'\d+', file.split('/')[-1])[0])))
    array.sort(key=lambda x: x[1], reverse=True)
    path, epoch = array[0]
    print(f'last state dict in {path}. epoch {epoch}')
    return path, epoch
