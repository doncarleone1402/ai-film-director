import torch
from torch.utils.data import Dataset
from transformers import GPT2Tokenizer


class ScriptsDataset(Dataset):
    def __init__(self, list_of_sentences):
        self.labels = []
        self.tokenizer = GPT2Tokenizer.from_pretrained('distilgpt2')
        self.tokenizer.pad_token = self.tokenizer.eos_token
        self.data = self.tokenizer(list_of_sentences, padding=True, return_attention_mask=True)
        labels = []
        for ids, attention_mask in zip(self.data['input_ids'], self.data['attention_mask']):
            label = ids.copy()
            real_len = sum(attention_mask)
            padding_len = len(attention_mask) - sum(attention_mask)
            label[:] = label[:real_len] + [-100]*padding_len
            labels.append(label)
        self.data['labels'] = labels
        print('length of sent: ', len(self.data['input_ids'][0]))
        print('length of data: ', len(self.data['input_ids']))

    def __len__(self):
        return len(self.data['input_ids'])

    def __getitem__(self, index):
        return [torch.tensor(self.data['input_ids'][index], dtype=torch.long),
                torch.tensor(self.data['attention_mask'][index], dtype=torch.long),
                torch.tensor(self.data['labels'][index], dtype=torch.long)]
