
from seldon_core.user_model import SeldonComponent
from transformers import GPT2Tokenizer, GPT2LMHeadModel
import torch
import logging
import numpy as np
from typing import Dict, Union, List, Optional

logger = logging.getLogger(__name__)
torch.set_num_threads(1)

class ScriptWriter(SeldonComponent):

    def __init__(self, chkpt_pth: str = "models/chkpt.pt", pretrained=False) -> None:
        self.chkpt_pth = chkpt_pth
        self.pretrained = pretrained
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.ready = False
        self.load()

    def load(self):
        if not self.ready:
            self.model = GPT2LMHeadModel.from_pretrained('distilgpt2')
            if self.pretrained:
                self.model.load_state_dict(torch.load(self.chkpt_pth))
            self.model.to(self.device)
            self.tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
            self.tokenizer.pad_token = self.tokenizer.eos_token
            self.ready = True
        else:
            logger.info("model is already loaded")

    def predict(
            self,
            X: Union[np.ndarray, List, str, bytes, Dict],
            names: Optional[List[str]]= ["big"],
            meta: Optional[Dict] = {}
        ) -> Union[np.ndarray, List, str, bytes, Dict]:
        dict_of_size = {'extrasmall':50, 'small': 100, 'middle': 150, 'big': 200}
        ids = self.tokenizer.encode(X, return_tensors='pt').to(self.device)
        greedy_output = self.model.generate(
                ids,
                max_length=dict_of_size[names[0]],
                do_sample=True,
                early_stopping=True,
                num_beans=3,
                temperature=0.8,
                top_k=40,
                top_p=0.95,
                num_return_sequences=1,
        )
        result = self.tokenizer.decode(greedy_output[0], skip_special_tokens=True)
        return result
