FROM python:3.8-slim-buster

COPY ./requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /home/app
COPY ./deploy ./
COPY ./models ./models
EXPOSE 5000 6000 9000

ENV SERVICE_TYPE MODEL
ENV MODEL_NAME ScriptGenerator
CMD exec seldon-core-microservice $MODEL_NAME --service-type $SERVICE_TYPE
