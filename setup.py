from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Package for deploying an AI-Script-Writer',
    author='Meshcheryakov Ilya, Samorodova Maria',
    license='',
)
